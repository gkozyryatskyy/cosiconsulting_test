package com.test.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.test.services.ApiService;
import com.test.services.ApiService.GroovyRequest;
import com.test.util.JsonUtils;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;

public class WebApi extends AbstractVerticle {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());
    protected HttpServer server;

    @Override
    public void start() throws Exception {
        server = vertx.createHttpServer(new HttpServerOptions().setIdleTimeout(30));
        Router router = Router.router(vertx);
        addRouts(router);
        router.route("/api-docs/*").handler(StaticHandler.create());
        server.requestHandler(router::accept).listen(8080, "localhost");
    }

    @Override
    public void stop() {
        server.close();
    }

    private void addRouts(Router router) {
        router.route().handler(BodyHandler.create());
        router.route().handler(c -> {
            c.response().putHeader("content-type", "application/json; charset=utf-8");
            c.next();
        });
        router.route().failureHandler(this::handleException);
        addApiRouts(router);
    }

    private void addApiRouts(Router router) {
        router.get("/api/calc").handler(c -> new ApiService(c).calc(Integer.parseInt(c.request().getParam("x1")),
                Integer.parseInt(c.request().getParam("x2"))));
        router.post("/api/groovy")
                .handler(c -> new ApiService(c).groovy(JsonUtils.read(c.getBodyAsString(), GroovyRequest.class)));
    }

    private void handleException(RoutingContext context) {
        Throwable cause = context.failure();
        log.error("Handler exception. ", cause);
        if (cause instanceof ReplyException) {
            ReplyException ex = (ReplyException) cause;
            replyError(context, ex.failureCode(), ex.getMessage());
        } else if (cause instanceof RuntimeException) {
            replyError(context, 400, cause.getMessage());
        } else {
            replyError(context, 500, cause.getMessage());
        }
    }

    private void replyError(RoutingContext context, int status, String message) {
        if (message != null) {
            context.response().setStatusCode(status).end(message);
        } else {
            context.response().setStatusCode(status).end();
        }
    }
}
