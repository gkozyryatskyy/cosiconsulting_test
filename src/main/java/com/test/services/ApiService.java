package com.test.services;

import java.util.Map;

import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.test.util.JsonUtils;

import groovy.lang.Binding;
import groovy.lang.Closure;
import groovy.lang.GroovyShell;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.vertx.ext.web.RoutingContext;

@Api(value = "ApiService")
public class ApiService {

    protected static final Logger log = LoggerFactory.getLogger(ApiService.class);

    private RoutingContext context;

    public ApiService(RoutingContext c) {
        this.context = c;
    }

    @Path("/calc")
    @ApiOperation(httpMethod = "GET", value = "calc", notes = "calculate x1 * x2", response = ApiResponse.class)
    public void calc(@QueryParam("x1") Integer x1, @QueryParam("x2") Integer x2) {
        log.info("Calculating x1 * x2");
        context.response().end(JsonUtils.writeValueAsString(new ApiResponse(x1 * x2)));
    }

    public static class ApiResponse {

        private Number response;

        public ApiResponse(Number response) {
            this.response = response;
        }

        public Number getResponse() {
            return response;
        }

        public void setResponse(Number response) {
            this.response = response;
        }
    }

    @Path("/groovy")
    @ApiOperation(httpMethod = "POST", value = "groovy", notes = "calculate simple scripted formula.", response = ApiResponse.class)
    public void groovy(GroovyRequest request) {
        log.info("Calculating {}", request);

        Binding binding = new Binding(request.getParams());
        GroovyShell sh = new GroovyShell(binding);
        Object object = sh.evaluate("return {" + request.getFormula() + "}");

        if (object instanceof Closure) {
            @SuppressWarnings("unchecked")
            Number closure = ((Closure<Number>) object).call();
            context.response().end(JsonUtils.writeValueAsString(new ApiResponse(closure)));
        } else {
            throw new IllegalArgumentException("Couldn't evaluate formula as closure. Result: " + object);
        }
    }

    public static class GroovyRequest {

        private Map<String, Integer> params;
        private String formula;

        public Map<String, Integer> getParams() {
            return params;
        }

        public void setParams(Map<String, Integer> params) {
            this.params = params;
        }

        public String getFormula() {
            return formula;
        }

        public void setFormula(String formula) {
            this.formula = formula;
        }

        @Override
        public String toString() {
            return "GroovyRequest [params=" + params + ", formula=" + formula + "]";
        }
    }
}
