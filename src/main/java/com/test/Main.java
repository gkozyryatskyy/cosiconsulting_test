package com.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.test.core.WebApi;

import io.vertx.core.Vertx;

public class Main {

    private static final Logger log = LoggerFactory.getLogger(Main.class);
    private static Vertx vertx;

    public static void main(String[] args) {
        vertx = Vertx.vertx();
        deployWeb();
    }

    private static void deployWeb() {
        vertx.deployVerticle(new WebApi(), res -> {
            if (res.succeeded()) {
                log.info("WebApi deployed. Deployment id is: " + res.result());
            } else {
                log.error("WebApi deployment exception. ", res.cause());
            }
        });
    }

}
