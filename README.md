# Build
----------------------------------------------
> mvn clean install

# Run
----------------------------------------------
> java -jar target/test-1.0.jar

# Api doc
----------------------------------------------

> http://localhost:8080/api-docs/

# You can test endpoint here
----------------------------------------------
> http://localhost:8080/api-docs/#!/ApiService/calc

> http://localhost:8080/api-docs/#!/ApiService/groovy

# Example
curl -XPOST -d '{ "params": {"x1" : 2, "x2" : 2}, "formula": "x1 * x2" }' 'http://localhost:8080/api/groovy'

curl -XPOST -d '{ "params": {"x1" : 2, "x2" : 2}, "formula": "x1 + 5*x2/x1" }' 'http://localhost:8080/api/groovy'